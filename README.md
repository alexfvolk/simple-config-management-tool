# Simple Config Management Tool

This is a simple python configuration management tool.

# Features!

- Use SSH to push your configuration to multiple remote hosts
- Intelligently copies files to remote host only if they require changes in content, permissions or owners
- Install, remove or upgrade apt packages
- Automatically restarts services by detecting if any packages or files were modified
- Idempotently apply the same changes over and over again
- Separate your configurations into their own folders
- No agent required on remote hosts


### Tech

This tool uses the following technologies:

- [Python2.7] - What the tool is written in
- [YAML] - defining all the things you want to configure (files, packages, services)
- [SSH] - How the tool connects to remote hosts
- [Various linux command line tools] - To start services, calculate MD5, diff files, install packages, etc

### Installation

You will need the following requirements met to run this tool:
- SSH key based authentication enabled for all the target remote hosts you want to configure
- Python2.7

Simply extract the package where you want:

```sh
$ tar -zxvf simple-config-management-tool.tar.gz
```

### Usage

- Get a list of servers you want to configure
- Write your config or use the sample config to install a simple web app (apache2 & php) to test
- Navigate into the simple-config-management folder and run the following command:

```sh
$ ./easy-config.py simple-php-app 34.207.251.106 3.80.91.254
```

### Writing your own configs

Every config has the following structure:
```
simple-config-management-tool
├── configs
│   └── simple-php-app
│       ├── files
│       │   └── hello.php
│       └── simple-php-app.yaml
├── easy-config.py
└── README
```

**Main script to run**
```
easy_config.py
```

**Usage**
```
usage: easy-config.py [-h] config hosts [hosts ...]

positional arguments:
  config      the config you want to use for managing host(s)
  hosts       the ips of the hosts you want to configure

optional arguments:
  -h, --help  show this help message and exit
```

**config** - this is the name of the folder where you define all your files you want to copy, main yaml file where you define what you want to configure. Think of it as a playbook or recipe. You can only run one at a time.

**hosts** - these are the ips of the hosts you want to configure. There is no parellel execution, the hosts are configured one at a time in sequence. Each host requires SSH key access.

**YAML file** - this is where you define packages you want to install, files you want to copy over, permissions you want to set, change group and user ownership.

Example:
```
files:
  - name: 'hello.php'
    remote_path: '/var/www/html/hello.php'
    user: 'root'
    group: 'root'
    permissions: '0644'
packages:
  - name: 'apache2'
    action: 'install'
  - name: 'php5'
    action: 'install'
services:
  - name: 'apache2'
    state: 'start'
```

Files:
- All files are placed under the files folder in the config folder
- The remote path defines where you want to place it on the remote host
- User and group define the owner of the file
- Permissions you want the file to have


Packages:
- Name of the package
- Action means if you want to install, remove or upgrade, similar to apt
- Make sure for actions you only put one of the following:
    - **install**
    - **remove**
    - **--only-upgrade install**

- The last one is for upgrading packages that are already installed and will not install it if not already there

Services:
- Name of service
- Action:
    - **start**
    - **stop**
    - **restart**
    - **reload**


- Technically anything that the service command takes as an argument (check usage)

**files/** folder where you place all the files you defined in the YAML file that you want to copy to the server

### License
----
MIT
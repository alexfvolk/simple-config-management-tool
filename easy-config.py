#!/usr/bin/python

import argparse
import yaml
import socket
import subprocess
import os


def check_connectivity(host, port):
    s = socket.socket()
    try:
        s.connect((host, port))
    except Exception as e:
        print('Unable to connect to {host} on port {port}: {error}'.format(
            host=host,
            port=port,
            error=e
        ))
    finally:
        s.close()


def check_ip(host):
    pass


def copy_file(local_path, remote_path, host, user='root'):
    # Check if directory exists on remote server, if not create it
    try:
        cmd = ['ssh', '{}@{}'.format(user, host), 'ls', os.path.dirname(remote_path), '&>', '/dev/null']
        run(cmd, host)
    except Exception as e:
        cmd = ['ssh', '{}@{}'.format(user, host), 'mkdir', '-p', os.path.dirname(remote_path)]
        run(cmd, host)
    # now you can safely copy the file
    print('File {} copied to {}:{}'.format(local_path, host, remote_path))
    cmd = ['rsync', '--checksum', local_path, '{}:{}'.format(host, remote_path)]
    run(cmd, host)


def set_permissions(remote_path, permissions, host, user='root'):
    print('File {} permissions changed to {}'.format(remote_path, permissions))
    cmd = ['ssh', '{}@{}'.format(user, host), 'chmod', permissions, remote_path]
    run(cmd, host)


def set_owners(remote_path, user_owner, group_owner, host, user='root'):
    print('File {} ownership changed to {}:{}'.format(remote_path, user_owner, group_owner))
    cmd = ['ssh', '{}@{}'.format(user, host), 'chown', '{}:{}'.format(user_owner, group_owner), remote_path]
    run(cmd, host)


def package_handler(package, action, host, user='root'):
    print('Preparing to {} {} on {}'.format(action, package, host))
    cmd = ['ssh', '{}@{}'.format(user, host), 'apt', action, '-y', package]
    run(cmd, host)


def run(cmd, host):
    p = subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    out = p.communicate()[0]
    return_code = p.returncode
    if not return_code == 0:
        raise Exception('Remote command \'{cmd}\' on {host} returned with non zero return code: {err}'.format(
            host=host,
            cmd=cmd,
            err=out
        ))
    return return_code, out


def trigger_services_restart(services, host, user='root'):
    for service in services:
        state = 'restart'
        cmd = ['ssh', '{}@{}'.format(user, host), 'service', service['name'], state]
        print('Restarting service {} on host {}'.format(service['name'], host))
        run(cmd, host)


def apply(host, config_data, user='root'):
    print('Checking connectivity with host {}'.format(host))
    check_ip(host)
    # Check for SSH only, since icmp might be blocked
    check_connectivity(host, 22)
    # Files (copy files over, set owners and permissions) Check everytime if file needs to be modified, otherwise
    # skip copying/replacing file(s).
    # To perform the check, the file will be copied to the remote host, set owner and permissions to match expected
    # owner and permissions on the temp file, and then calculate the md5sum of file content with the perms and
    # owners of the file together, then compare to the file already existing on the host to see if it need to change.
    for file in config_data['files']:
        exists_file = False
        file_needs_update = False
        files_updated = False
        try:
            cmd = ['ssh', '{}@{}'.format(user, host), 'ls', file['remote_path']]
            run(cmd, host)
            exists_file = True
            temp_file_name = '{}{}'.format(file['name'], '.temp')
            remote_temp_file_path = os.path.join(os.path.dirname(file['remote_path']), temp_file_name)
            copy_file(os.path.join(FILES_DIR, file['name']), remote_temp_file_path, host)
            set_permissions(remote_temp_file_path, file['permissions'], host)
            set_owners(remote_temp_file_path, file['user'], file['user'], host)
            cmd = ['ssh',
                   '{}@{}'.format(user, host),
                   'cat',
                   file['remote_path'],
                   '<(stat {} | grep \'Access: (\')'.format(file['remote_path']),
                   '| md5sum |',
                   'cut -d \' \' -f 1']
            md5sum_original_file = run(cmd, host)[1].rstrip()
            cmd = ['ssh',
                   '{}@{}'.format(user, host),
                   'cat',
                   remote_temp_file_path,
                   '<(stat {} | grep \'Access: (\')'.format(remote_temp_file_path),
                   '| md5sum |',
                   'cut -d \' \' -f 1']
            md5sum_new_file = run(cmd, host)[1].rstrip()
            print('Compraing temp file with original file on host to see if we need any changes')
            print('{} {}'.format(md5sum_original_file, file['remote_path']))
            print('{} {}'.format(md5sum_new_file, remote_temp_file_path))
            if md5sum_original_file != md5sum_new_file:
                file_needs_update = True
        except Exception as e:
            exists_file = False
        if not exists_file or file_needs_update:
            copy_file(os.path.join(FILES_DIR, file['name']), file['remote_path'], host)
            set_permissions(file['remote_path'], file['permissions'], host)
            set_owners(file['remote_path'], file['user'], file['user'], host)
            files_updated = True
        else:
            files_updated=False
            print('Files are up to date')
    # Make copy of apt history
    # Install, upgrade, remove packages
    # Then compare apt history files to see if any changes were made
    apt_history = '/var/log/apt/history.log'
    apt_history_old = '/var/log/apt/history-old.log'
    exists_apt_history_file = False
    try:
        cmd = ['ssh', '{}@{}'.format(user, host), 'cp', apt_history, apt_history_old]
        run(cmd, host)
        exists_apt_history_file = True
    except Exception:
        # history file does not exist
        exists_apt_history_file = False
    for package in config_data['packages']:
        package_handler(package['name'], package['action'], host)
    # Detect if any packages were changed
    packages_modified = False
    try:
        cmd = ['ssh', '{}@{}'.format(user, host), 'diff', apt_history, apt_history_old]
        run(cmd, host)
        print('No packages were changed on {}'.format(host))
    except Exception:
        print('Packages were changed on {}'.format(host))
        packages_modified = True
    if packages_modified or files_updated:
        trigger_services_restart(config_data['services'], host)
    # Put services in correct state as per config
    for service in config_data['services']:
        service_started = False
        try:
            # Assume service is not started if return code not 0
            cmd = ['ssh', '{}@{}'.format(user, host), 'service', service['name'], 'status']
            run(cmd, host)
            service_started = True
        except Exception:
            service_started = False
        if service['state'] == 'start' and service_started:
            pass
        else:
            cmd = ['ssh', '{}@{}'.format(user, host), 'service', service['name'], service['state']]
            print('{} {} on host {}'.format(service['name'], service['state'], host))
            run(cmd, host)


parser = argparse.ArgumentParser()
parser.add_argument('config', help='the config you want to use for managing host(s)')
parser.add_argument('hosts', nargs='+', help='the ips of the hosts you want to configure')
args = parser.parse_args()
WORKING_DIR = os.path.dirname(os.path.abspath(__file__))
FILES_DIR = os.path.join(WORKING_DIR, 'configs', args.config, 'files')
yaml_file = 'configs/{}/{}.yaml'.format(args.config, args.config)
print('Loading desired config from yaml file: {}'.format(yaml_file))

with open(yaml_file) as f:
    config_data = yaml.load(f)
print('Hosts: ' + ', '.join(args.hosts))


for host in args.hosts:
    apply(host, config_data)